# jujuApp

#### 介绍
jujuApp 是一款基于vue + framework7 UI + cordova 框架开发的app，可web h5运行，可打包成android客户端，IOS客户端的一款混合APP应用；
功能列表：

#### 软件架构
* 前端： vue + framework7 UI + cordova  集成图片上传，地图选择；集成cordova插件，照片上传；地区选择组件；
* 后端： springBoot + bootstrap UI + ibatis框架

#### 应用截图

| ![](screenshot/2.png)| ![](screenshot/3.png)|
| ---- | ---- |
| ![](screenshot/4.png)| ![](screenshot/5.png)|
| ![](screenshot/6.png)| ![](screenshot/7.png)|
| ![](screenshot/8.png)| ![](screenshot/9.png)|
| ![](screenshot/10.png)| ![](screenshot/11.png)|
| ![](screenshot/12.png)| ![](screenshot/13.png)|
| ![](screenshot/14.png)| ![](screenshot/15.png)|
| ![](screenshot/16.png)| ![](screenshot/17.png)|
| ![](screenshot/18.png)| ![](screenshot/1.png)|


#### 安装教程

A full-featured Framework7 Vue with Webpack setup with hot-reload & css extraction. Based on [Vue Webpack Boilerplate](https://github.com/vuejs-templates/webpack)

## Usage

### 1. Download this repository
```
git clone https://gitee.com/xww520/jujuApp.git
```

Repository will be downloaded into `juju-ui/` folder

### 2. Instal dependencies

Go to the downloaded repository folder and run:
```
npm install
```

This will download latest version of Framework7, Framework7-Vue, Vue and required icon fonts (to `/src/fonts/`)

### 3. Run the app

```
npm run dev
```

如需修改IP和端口，请修改此处 webpack.config.dev.js
```
devServer: {
// host: '10.0.62.121', // can be overwritten by process.env.HOST
host: '192.168.124.85', // can be overwritten by process.env.HOST
port:8888,
hot: true,
open: true,
compress: true,
contentBase: '/www/',
watchOptions: {
poll: true
}
},
```
App will be opened in browser at `http://localhost:80/`

### 4. Build app for production

```
npm run build
```

The output will be at `www/` folder

## Use with cordova
Cordova is not enabled by default, so make sure to remove the comment tags around the `<script src="cordova.js"></script>` line in [projectroot]/src/index.html
```
<body>
  <div id="app"></div>

  <!-- Cordova -->

  <script src="cordova.js"></script>

  <!-- built script files will be auto injected -->
</body>
```
It will be added during the build process to Android/iOS.

Just put the contents of `www` folder in your cordova's project root `www` folder

## One command install

```
git clone https://github.com/david1688/juju-ui my-app &&
cd my-app &&
npm install &&
npm run dev
```

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for development
npm run build-dev

# build for production with minification
npm run build-prod
```

## Project Structure

* `src/index.html` - main app HTML
* `src/assets` - folder with static assets (images)
* `src/components` - folder with custom `.vue` components
* `src/css` - put custom app CSS styles here. Don't forget to import them in `main.js`
* `src/pages` - app `.vue` pages
* `src/app.js` - main app file where you include/import all required libs and init app
* `src/routes.js` - app routes
* `src/app.vue` - main app structure/component
* `/static/` - folder with extra static assets that will be copied into output folder


#### cordova打包说明

1. 如需打包成android客户端或ios，请查阅cordova官网相关步骤，或者百度很多资料

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)