import Mock from 'mockjs';

const vehicle = Mock.mock(
    '/api/vehicle', 'post', (req, res) => {
        return {
            code: 200,
            data: [{
                id: 1,
                licNumber: '陕A79898',
                color: 1,
                buyTime: '2017-04-01'

            }, {
                id: 1,
                licNumber: '陕A79898',
                color: 1,
                buyTime: '2017-04-01'

            }],
            message: '查询成功'
        }
    })
const member = Mock.mock(
    '/api/member/list', 'get', (req, res) => {
        return {
            code: 200,
            data: [{
                id: 1,
                name: '我是刘卫',
                age: 25,
                createTime: '2017-04-01'
            },{
                id: 1,
                name: '我是刘卫2',
                age: 252,
                createTime: '2017-04-01'
            }],
            message: '查询成功'
        }
    })

export default {vehicle, member}
