import request from '../utils/request'

const moduleUrl = "/api/pwd/";

/**
 * 获取验证码-忘记密码
 */
export function getValidCodeForRestPwd(loginName, imgCode, bizType) {
    return request({
        url: moduleUrl + 'getValidCodeForRestPwd',
        method: 'post',
        params: {loginName: loginName, imgCode: imgCode, bizType: bizType}
    })
}

/**
 * 验证码是否正确-忘记密码
 * @param data
 */
export function validateCodeForRestPwd(loginName, mobileCode, bizType) {
    return request({
        url: moduleUrl + 'validateCodeForRestPwd',
        method: 'post',
        params: {loginName: loginName, mobileCode: mobileCode, bizType: bizType}
    })
}

/**
 * 修改密码接口
 * @param params
 */
export function resetPwd(mobileCode, data) {
    return request({
        url: moduleUrl + 'resetPwd',
        method: 'post',
        params: {mobileCode: mobileCode},
        data: data,
    })
}


