import request from '../utils/request'

const moduleUrl = "/api/feedback/";

// 评论活动
export function insert(data) {
    return request({
        url: moduleUrl + 'insert',
        method: 'post',
        data: data
    })
}

// 上传反馈图片
export function uploadFeedImg(data, params) {
    return request({
        url: moduleUrl + 'uploadFeedImg',
        method: 'post',
        data: data,
        params: params
    })
}
