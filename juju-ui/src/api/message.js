import request from '../utils/request'

const moduleUrl = "/auth/api/message/";

/**
 * 获取消息page
 * @param params
 */
export function page(data, params) {
    return request({
        url: moduleUrl + 'page',
        method: 'post',
        data: data,
        params: params
    })
}
