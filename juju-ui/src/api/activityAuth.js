import request from '../utils/request'

const moduleUrl = "/auth/api/act/";

export function save(data) {
    return request({
        url: moduleUrl + 'save',
        method: 'post',
        data: data
    })
}

/*export function updateDesc(data) {
  return request({
    url: '/auth/api/act/updateDesc',
    method: 'post',
    data: data
  })
}*/

export function update(data) {
    return request({
        url: moduleUrl + 'update',
        method: 'post',
        data: data
    })
}

/**
 * 发布
 * @param data
 */
export function publish(data) {
    return request({
        url: moduleUrl + 'publish',
        method: 'post',
        data: data
    })
}

/**
 * 暂存
 * @param data
 */
export function beginSave(data) {
    return request({
        url: moduleUrl + 'beginSave',
        method: 'post',
        data: data
    })
}

/**
 * 我添加的聚会列表
 */
export function pageToMePublish(data, params) {
    return request({
        url: moduleUrl + 'pageToMePublish',
        method: 'post',
        data: data,
        params: params
    })
}

// 跟ta共同参与的活动
export function pageToJoinAndOtherAct(data, params) {
    return request({
        url: moduleUrl + 'pageToJoinAndOtherAct',
        method: 'post',
        data: data,
        params: params
    })
}

// 我收到的邀约活动
export function pageToMeByOffer(data, params) {
    return request({
        url: moduleUrl + 'pageToMeByOffer',
        method: 'post',
        data: data,
        params: params
    })
}


// 可评价活动
export function pageToCanCommentAct(data, params) {
    return request({
        url: moduleUrl + 'pageToCanCommentAct',
        method: 'post',
        data: data,
        params: params
    })
}

// 我参与的活动
export function pageToMyJoinAct(data, params) {
    return request({
        url: moduleUrl + 'pageToMyJoinAct',
        method: 'post',
        data: data,
        params: params
    })
}

// 活动收藏
export function pageByCollectAct(data, params) {
    return request({
        url: moduleUrl + 'pageByCollectAct',
        method: 'post',
        data: data,
        params: params
    })
}

// 报名活动提交
export function joinAct(data) {
    return request({
        url: moduleUrl + 'joinAct',
        method: 'post',
        data: data
    })
}

// 收藏活动
export function collectAct(data) {
    return request({
        url: moduleUrl + 'collectAct',
        method: 'post',
        data: data
    })
}

// 取消收藏活动
export function unCollectAct(data) {
    return request({
        url: moduleUrl + 'unCollectAct',
        method: 'post',
        data: data
    })
}






