import request from '@/utils/request'

const moduleUrl = "/api/searchRec/";

/**
 * 搜索
 */
export function list(data) {
    return request({
        url: moduleUrl + 'list',
        method: 'post',
        data: data
    })
}

/**
 * 搜索
 */
export function searchByKey(data, params) {
    return request({
        url: moduleUrl + 'searchByKey',
        method: 'post',
        data: data,
        params: params
    })
}

