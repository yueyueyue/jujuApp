import request from '../utils/request'

const moduleUrl = "/auth/api/taskYetRec/";

// 做任务
export function insert(data) {
    return request({
        url: moduleUrl + 'insert',
        method: 'post',
        data: data
    })
}

// 我的任务列表
export function pageToMe(data, params) {
    return request({
        url: moduleUrl + 'pageToMe',
        method: 'post',
        data: data,
        params: params
    })
}
