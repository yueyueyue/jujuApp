import request from '@/utils/request'

const moduleUrl = "/api/home/";

/**
 * 获取首页数据
 */
export function getHomeData(data, params) {
    return request({
        url: moduleUrl + 'getHomeData',
        method: 'post',
        data: data,
        params: params
    })
}

