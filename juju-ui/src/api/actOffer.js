import request from '../utils/request'

const moduleUrl = "/auth/api/actOffer/";

// 邀约用户聚会
export function insert(data) {
    return request({
        url: moduleUrl + 'insert',
        method: 'post',
        data: data
    })
}
