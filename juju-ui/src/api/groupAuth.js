import request from '../utils/request'

const moduleUrl = "/auth/api/group/";

export function save(data) {
    return request({
        url: moduleUrl + 'save',
        method: 'post',
        data: data
    })
}

export function update(data) {
    return request({
        url: moduleUrl + 'update',
        method: 'post',
        data: data
    })
}

/**
 * 发布
 * @param data
 */
export function publish(data) {
    return request({
        url: moduleUrl + 'publish',
        method: 'post',
        data: data
    })
}

/**
 * 暂存
 * @param data
 */
export function beginSave(data) {
    return request({
        url: moduleUrl + 'beginSave',
        method: 'post',
        data: data
    })
}

/**
 * 我发布的群
 */
export function pageGroupByMe(data, params) {
    return request({
        url: moduleUrl + 'pageGroupByMe',
        method: 'post',
        data: data,
        params: params
    })
}





