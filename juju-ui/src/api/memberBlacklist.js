import request from '../utils/request'

const moduleUrl = "/auth/api/memberBlacklist/";


// 黑名单列表
export function pageByMe(data, params) {
    return request({
        url: moduleUrl + 'pageByMe',
        method: 'post',
        data: data,
        params: params
    })
}

// 插入
export function insert(data) {
    return request({
        url: moduleUrl + 'insert',
        method: 'post',
        data: data
    })
}

// 移除黑名单
export function deleteBlacklist(data) {
    return request({
        url: moduleUrl + 'deleteBlacklist',
        method: 'post',
        data: data
    })
}
