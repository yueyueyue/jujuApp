import request from '../utils/request'

const moduleUrl = "/auth/api/memberComment/";

// 评论会员
export function insert(data) {
    return request({
        url: moduleUrl + 'insert',
        method: 'post',
        data: data
    })
}

// 我收到的评价
export function pageCommentByMe(data, params) {
    return request({
        url: moduleUrl + 'pageCommentByMe',
        method: 'post',
        data: data,
        params: params
    })
}
