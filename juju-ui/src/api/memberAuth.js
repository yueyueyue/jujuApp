import request from '../utils/request'

const moduleUrl = "/auth/api/member/";

/**
 * 获取个人信息
 * @param params
 */
export function getMyMemberInfo() {
    return request({
        url: moduleUrl + 'getMyMemberInfo',
        method: 'post',
    })
}

/**
 * 实名认证
 * @param params
 */
export function realNameAuth(data) {
    return request({
        url: moduleUrl + 'realNameAuth',
        method: 'post',
        data: data
    })
}

/**
 * 会员更新
 * @param params
 */
export function update(data) {
    return request({
        url: moduleUrl + 'update',
        method: 'post',
        data: data
    })
}

/**
 * 上传头像
 * @param params
 */
export function uploadHeadPic(data, config) {
    return request({
        url: moduleUrl + 'uploadHeadPic',
        config: config,
        method: 'post',
        params: data
    })
}

/**
 * 注册时候，上传头像和颜值扫描
 * @param params
 */
export function uploadHeadPicAndScan(data, config) {
    return request({
        url: moduleUrl + 'uploadHeadPicAndScan',
        config: config,
        method: 'post',
        params: data
    })
}

/**
 * 获取粉丝
 * @param data
 * @param params
 */
export function getFans(data, params) {
    return request({
        url: moduleUrl + 'getFans',
        method: 'post',
        data: data,
        params: params
    })
}

/**
 * 获取关注
 * @param data
 * @param params
 */
export function getFocus(data, params) {
    return request({
        url: moduleUrl + 'getFocus',
        method: 'post',
        data: data,
        params: params
    })
}

// 关注会员
export function focusMember(data) {
    return request({
        url: moduleUrl + 'focusMember',
        method: 'post',
        data: data
    })
}

// 取消关注会员
export function unFocusMember(data) {
    return request({
        url: moduleUrl + 'unFocusMember',
        method: 'post',
        data: data
    })
}

// 收藏会员
export function collectMember(data) {
    return request({
        url: moduleUrl + 'collectMember',
        method: 'post',
        data: data
    })
}

// 取消收藏会员
export function unCollectMember(data) {
    return request({
        url: moduleUrl + 'unCollectMember',
        method: 'post',
        data: data
    })
}

// 评价会员
export function commentMember(data) {
    return request({
        url: moduleUrl + 'commentMember',
        method: 'post',
        data: data
    })
}

// 可评价的会员（是已参与，并已结束的活动）
export function pageToCanCommentMember(data, params) {
    return request({
        url: moduleUrl + 'pageToCanCommentMember',
        method: 'post',
        data: data,
        params: params
    })
}

// 我的会员收藏
export function pageByCollectMember(data, params) {
    return request({
        url: moduleUrl + 'pageByCollectMember',
        method: 'post',
        data: data,
        params: params
    })
}

// 用户中心-各聚会，关注，收藏，消息，评价总数
export function getCenterAllSum(data) {
    return request({
        url: moduleUrl + 'getCenterAllSum',
        method: 'post',
        data: data
    })
}

// 群列表页面-收到的邀约总数，参与的报名通过总数，发起的聚会审核通过总数
export function getActPageAllSum(data) {
    return request({
        url: moduleUrl + 'getActPageAllSum',
        method: 'post',
        data: data
    })
}




