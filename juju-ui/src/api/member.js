import request from '../utils/request'

const moduleUrl = "/api/member/";

/**
 * 获取单个会员
 * @param params
 */
export function getMember(data) {
    return request({
        url: moduleUrl + 'getMember.short',
        method: 'post',
        params: data
    })
}

/**
 * 会员列表
 */
export function pageMember(data, params) {
    return request({
        url: moduleUrl + 'pageMember.short',
        method: 'post',
        data: data,
        params: params
    })
}

/**
 * 附近的人
 */
export function pageByNear(data, params) {
    return request({
        url: moduleUrl + 'pageByNear.short',
        method: 'post',
        data: data,
        params: params
    })
}

/**
 * 用户标签
 */
export function pageMemberLabel(data, params) {
    return request({
        url: moduleUrl + 'pageMemberLabel.short',
        method: 'post',
        data: data,
        params: params
    })
}

// 活动已报人员
export function yetJoinActMember(data, params) {
    return request({
        url: moduleUrl + 'yetJoinActMember',
        method: 'post',
        data: data,
        params: params
    })
}




