import request from '../utils/request'

const moduleUrl = "/api/actType/";

/**
 * 获取活动类型
 * @param data
 */
export function pageActTypeList(data) {
    return request({
        url: moduleUrl + 'pageActTypeList',
        method: 'post',
        data: data
    })
}





