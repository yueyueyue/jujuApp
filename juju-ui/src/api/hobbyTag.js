import request from '../utils/request'

const moduleUrl = "/api/hobbyTag/";

export function list(data, params) {
    return request({
        url: moduleUrl + 'list',
        method: 'post',
        data: data,
        params: params
    })
}

