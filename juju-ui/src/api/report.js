import request from '../utils/request'

const moduleUrl = "/auth/api/report/";

// 举报会员
export function insert(data) {
    return request({
        url: moduleUrl + 'insert',
        method: 'post',
        data: data
    })
}
