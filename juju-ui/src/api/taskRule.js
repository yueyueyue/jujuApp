import request from '../utils/request'

const moduleUrl = "/api/taskRule/";

// 任务列表
export function page(data, params) {
    return request({
        url: moduleUrl + 'page',
        method: 'post',
        data: data,
        params: params
    })
}
