import axios from 'axios'
import {getToken} from '../utils/auth'

// 上传通用封装
export function upload(file, targetPath) {
    let param = new FormData(); //创建form对象
    param.append('file', file);//通过append向form对象添加数据
    console.log(param.get('file')); //FormData私有类对象，访问不到，可以通过get判断值是否传进去
    let config = {
        headers: {'Content-Type': 'multipart/form-data'},
    };
    // 添加请求头
    config.headers['Authorization'] = getToken();
    return axios.post(targetPath, param, config);
}






