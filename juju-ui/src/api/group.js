import request from '../utils/request'

const moduleUrl = "/api/group/";

/**
 * pageGroup
 */
export function pageGroup(data, params) {
    return request({
        url: moduleUrl + 'pageGroup.short',
        method: 'post',
        data: data,
        params: params
    })
}

export function get(data) {
    return request({
        url: moduleUrl + 'get',
        method: 'post',
        data: data
    })
}






