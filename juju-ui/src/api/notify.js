import request from '../utils/request'

const moduleUrl = "/api/notifyMsg/";

export function page(data) {
    return request({
        url: moduleUrl + 'page',
        method: 'post',
        data: data
    })
}

