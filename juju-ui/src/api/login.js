import request from '../utils/request'

const moduleUrl = "/api/login/";

/**
 * 登录
 */
export function login(data) {
    return request({
        url: moduleUrl + 'login',
        method: 'post',
        data: data
    })
}

