import request from '../utils/request'

const moduleUrl = "/auth/api/actComment/";

// 评论活动
export function insert(data) {
    return request({
        url: moduleUrl + 'insert',
        method: 'post',
        data: data
    })
}

// 查询活动评论
export function pageActComment(data, params) {
    return request({
        url: moduleUrl + 'pageActComment',
        method: 'post',
        data: data,
        params: params
    })
}
