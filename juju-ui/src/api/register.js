import request from '../utils/request'

const moduleUrl = "/api/register/";

/**
 * 获取验证码
 */
export function getValidCode(loginName, imgCode, bizType) {
    return request({
        url: moduleUrl + 'getValidCode',
        method: 'post',
        params: {loginName: loginName, imgCode: imgCode, bizType: bizType}
    })
}

/**
 * 验证码是否正确
 * @param data
 */
export function validateCode(loginName, mobileCode, bizType) {
    return request({
        url: moduleUrl + 'validateCode',
        method: 'post',
        params: {loginName: loginName, mobileCode: mobileCode, bizType: bizType}
    })
}

/**
 * 注册接口
 * @param params
 */
export function register(mobileCode, data) {
    return request({
        url: moduleUrl + 'register',
        method: 'post',
        params: {mobileCode: mobileCode},
        data: data,
    })
}



