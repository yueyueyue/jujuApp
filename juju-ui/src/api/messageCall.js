import request from '../utils/request'

const moduleUrl = "/auth/api/messageCall/";

/**
 * 打招呼
 */
export function insert(data) {
    return request({
        url: moduleUrl + 'insert',
        method: 'post',
        data: data
    })
}

/**
 * 我收到的招呼
 */
export function pageToMyAccept(data, params) {
    return request({
        url: moduleUrl + 'pageToMyAccept',
        method: 'post',
        data: data,
        params: params
    })
}

/**
 * 我发出的招呼
 */
export function pageToMySend(data, params) {
    return request({
        url: moduleUrl + 'pageToMySend',
        method: 'post',
        data: data,
        params: params
    })
}


