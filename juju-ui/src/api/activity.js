import request from '../utils/request'

const moduleUrl = "/api/act/";

export function get(data) {
    return request({
        url: moduleUrl + 'get.short',
        method: 'post',
        data: data
    })
}

/**
 * 获取活动类型
 * @param data
 */
export function getActTypeList(data) {
    return request({
        url: moduleUrl + 'getActTypeList',
        method: 'post',
        data: data
    })
}

/**
 * 聚会列表
 */
export function pageAct(data, params) {
    return request({
        url: moduleUrl + 'pageAct.short',
        method: 'post',
        data: data,
        params: params
    })
}

/**
 * 附近活动
 * @param data
 * @param params
 */
export function pageActNear(data, params) {
    return request({
        url: moduleUrl + 'pageActNear.short',
        method: 'post',
        data: data,
        params: params
    })
}

/**
 * 人气聚会
 * @param data
 * @param params
 */
export function pageActTop(data, params) {
    return request({
        url: moduleUrl + 'pageActTop.short',
        method: 'post',
        data: data,
        params: params
    })
}

/**
 * 自驾游
 * @param data
 * @param params
 */
export function pageActDriving(data, params) {
    return request({
        url: moduleUrl + 'pageActDriving.short',
        method: 'post',
        data: data,
        params: params
    })
}

/**
 * 周末活动
 * @param data
 * @param params
 */
export function pageActWeekend(data, params) {
    return request({
        url: moduleUrl + 'pageActWeekend.short',
        method: 'post',
        data: data,
        params: params
    })
}

/**
 * 根据活动分类ID获取活动
 * @param data
 * @param params
 */
export function pageActByActTypeId(data, params) {
    return request({
        url: moduleUrl + 'pageActByActTypeId.short',
        method: 'post',
        data: data,
        params: params
    })
}





