import request from '../utils/request'

const moduleUrl = "/api/groupType/";

/**
 * 所有群分类
 */
export function pageGroupTypeList(data, params) {
    return request({
        url: moduleUrl + 'pageGroupTypeList',
        method: 'post',
        data: data,
        params: params
    })
}


