import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import user from './modules/user'
import getters from './getters'

// import uploader from "vue-easy-uploader";

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    user
  },
  getters
})

// 上传组件需要使用store
// Vue.use(uploader, store)

export default store
