import {getToken, setToken, removeToken,setStore} from '@/utils/auth'
import {login} from '@/api/login'
import {register} from '@/api/register'
import Cookies from 'js-cookie'

const user = {
    state: {
        token: getToken(),
        registrationID: Cookies.get('registrationID'),
        loginName: '',
        userName: '',
        headPic: Cookies.get('headPic'),
        photos: '',
        sex: '',
        userLevel: '',
        sign: '',
        city: '',
        cityName: '',
        faceScore: '',
        age: '',
        hobTag:'',
        registerNo: '',
        contributionScore: '',
        socialScore: '',
        integralScore: '',
        goHome: '',
        affectiveState: '',
        canRealState: '',
        postJob: '',
        birthday: '',
        height: '',
        state: '',
        email: '',
        recemCode:''
    },

    mutations: {
        SET_TOKEN: (state, token) => {
            state.token = token
        },
        SET_USER_INFO: (state, userInfo) => {
            state.registrationID = Cookies.get('registrationID');
            state.loginName = userInfo.loginName;
            state.userName = userInfo.userName;
            state.headPic = userInfo.headPic;
            // 保存一份到cookie,下次不用直接登录展示头像
            Cookies.set("headPic",userInfo.headPic);
            state.photos = userInfo.photos;
            state.sex = userInfo.sex;
            state.userLevel = userInfo.userLevel;
            state.sign = userInfo.sign;
            state.city = userInfo.city;
            state.cityName = userInfo.cityName;
            state.faceScore = userInfo.faceScore;
            state.age = userInfo.age;
            state.hobTag = userInfo.hobTag;
            state.registerNo = userInfo.registerNo;
            state.contributionScore = userInfo.contributionScore;
            state.contributionScore = userInfo.contributionScore;
            state.socialScore = userInfo.socialScore;
            state.integralScore = userInfo.integralScore;
            state.goHome = userInfo.goHome;
            state.canRealState = userInfo.canRealState;
            state.postJob = userInfo.postJob;
            state.birthday = userInfo.birthday;
            state.height = userInfo.height;
            state.state = userInfo.state;
            state.email = userInfo.email;
            state.recemCode = userInfo.recemCode;
        },
        SET_HOBBY_TAGS: (state, str) => {
            state.hobTag = str;
        },
        SET_HEAD_PIC: (state, str) => {
            state.headPic = str;
        },
        SET_PHOTO_PIC: (state, str) => {
            state.photos = str;
        },
    },

    actions: {
        // 登录
        Login({commit}, memberBase) {
            const token = memberBase.token;
            setToken(token);
            commit('SET_TOKEN', token);
            commit('SET_USER_INFO', memberBase);
        },

        // 设置用户信息
        setUserInfo({commit}, memberBase) {
            commit('SET_USER_INFO', memberBase);
        },
        setHobbyTags({commit}, str) {
            commit('SET_HOBBY_TAGS', str);
        },

        setHeadPic({commit}, str) {
            commit('SET_HEAD_PIC', str);
        },
        setPhotoPic({commit}, str) {
            commit('SET_PHOTO_PIC', str);
        },

        // 注册,赋值到state
        Register({commit}, memberBase) {
            const token = memberBase.token;
            setToken(token);
            commit('SET_TOKEN', token);
            commit('SET_USER_INFO', memberBase);
        },

        // 登出
        LogOut({commit, state}) {
            commit('SET_TOKEN', '');
            removeToken();
        },

    }
}

export default user
