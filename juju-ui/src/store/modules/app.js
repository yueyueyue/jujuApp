import Cookies from 'js-cookie'

const app = {
  state: {

    colorTheme: Cookies.get('colorTheme'),
    nightMode: Cookies.get('nightMode'),
    locationCity: Cookies.get('locationCity'),
    locationCityCode: Cookies.get('locationCityCode'),

    sidebar: {
      opened: !+Cookies.get('sidebarStatus'),
      withoutAnimation: false
    },
    device: 'desktop'
  },
  mutations: {
    // 切换主题
    CHANGE_COLOR_THEME: (state, colorTheme) => {
      state.colorTheme = colorTheme;
      Cookies.set('colorTheme',colorTheme);
    },

    // 改变城市定位
    CHANGE_LOCATION_CITY: (state, locationCity) => {
      state.locationCity = locationCity.name;
      state.locationCityCode = locationCity.code;
      Cookies.set('locationCity', locationCity.name);
      Cookies.set('locationCityCode', locationCity.code);
    },

    // 夜间模式
    CHANGE_NIGHT_MODE: (state, nightMode) => {
      state.nightMode = nightMode;
      Cookies.set('nightMode', nightMode);
    },

    TOGGLE_SIDEBAR: state => {
      if (state.sidebar.opened) {
        Cookies.set('sidebarStatus', 1)
      } else {
        Cookies.set('sidebarStatus', 0)
      }
      state.sidebar.opened = !state.sidebar.opened
    },
    CLOSE_SIDEBAR: (state, withoutAnimation) => {
      Cookies.set('sidebarStatus', 1)
      state.sidebar.opened = false
      state.sidebar.withoutAnimation = withoutAnimation
    },
    TOGGLE_DEVICE: (state, device) => {
      state.device = device
    }
  },
  actions: {
    changeColorTheme({commit}, colorTheme) {
      commit('CHANGE_COLOR_THEME', colorTheme)
    },
    changeNightMode({commit}, nightMode) {
      commit('CHANGE_NIGHT_MODE', nightMode)
    },
    // 改变定位信息
    changeLocationCity({commit}, locationCity) {
      commit('CHANGE_LOCATION_CITY', locationCity)
    },

    ToggleSideBar: ({ commit }) => {
      commit('TOGGLE_SIDEBAR')
    },
    CloseSideBar({ commit }, { withoutAnimation }) {
      commit('CLOSE_SIDEBAR', withoutAnimation)
    },
    ToggleDevice({ commit }, device) {
      commit('TOGGLE_DEVICE', device)
    }
  }
}

export default app
