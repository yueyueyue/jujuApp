// Import Vue
import Vue from 'vue';

// Import F7
import Framework7 from 'framework7/framework7.esm.bundle.js';

// Import F7 Vue Plugin
import Framework7Vue from 'framework7-vue/framework7-vue.esm.bundle.js';

// Import F7 Styles
import 'framework7/css/framework7.bundle.css';

// Import Icons and App Custom Styles
import IconsStyles from './css/icons.css';
import AppStyles from './css/app.css';

// import './common/eleditor/jquery.min.js'

// import $ from 'jquery'

// Import App Component

import App from './app.vue';

// Init F7 Vue Plugin
Framework7.use(Framework7Vue)

// import Vuex from 'vuex'
// Vue.use(Vuex)

import store from './store'

// import uploader from 'vue-easy-uploader'

// let store = new Vuex.Store({})
// Vue.use(uploader, store)

import global_ from './utils/global'
Vue.prototype.GLOBAL = global_

import 'font-awesome/css/font-awesome.min.css'
import initRichText from './common/initHTMLEditor';
initRichText();

// import mockdata from "./mockjs"

import VueCropper from 'vue-cropper'
Vue.use(VueCropper)

// 百度地图
import BaiduMap from 'vue-baidu-map'
Vue.use(BaiduMap, {
  // ak 是在百度地图开发者平台申请的密钥 详见 http://lbsyun.baidu.com/apiconsole/key */
  ak: 'bE3WvFXmETRAULQrxvFQ3nX1ybj9jxOf'
})

// Init App
new Vue({
  el: '#app',
  template: '<app/>',
  store,
  // Register App Component
  components: {
    app: App,
  },
  methods:{

  },
});



