/**
 * 所有路由定义
 */
// import CounterControls from './pages/test/CounterControls.vue';

// common
import citySelect from './pages/common/citySelect.vue';
import cityCheck from './pages/common/cityCheck.vue';
import locationSelect from './pages/common/locationSelect.vue';

// system
import about from './pages/system/about.vue';
import feedback from './pages/system/feedback.vue';
import notFound from './pages/system/notFound.vue';
import settingList from './pages/system/settingList.vue';
import changeTheme from './pages/system/changeTheme.vue';

// 通知
import notifyList from './pages/notify/notifyList.vue';

// 启动页
import guideHome from './pages/guide/guideHome.vue';

// 广场
import plazaHome from './pages/plaza/plazaHome.vue';
import plazaAdd from './pages/plaza/plazaAdd.vue';
import plazaComment from './pages/plaza/plazaComment.vue';


// 群
import groupAdd from './pages/group/groupAdd.vue';
import groupAddDesc from './pages/group/groupAddDesc.vue'
import groupCoverUpload from './pages/group/groupCoverUpload.vue'
import groupDetailPre from './pages/group/groupDetailPre.vue'
import groupList from './pages/group/groupList.vue'
import groupTypeList from './pages/group/groupTypeList.vue'
import myGroupList from './pages/group/myGroupList.vue'
import groupDetail from './pages/group/groupDetail.vue'

// 首页
import homeTabbar from './pages/homeTabbar.vue';
import home from './pages/home.vue';
import SearchReady from './pages/searchReady.vue';

// 会员
import auditMember from './pages/member/auditMember.vue';
import auditSuccessMember from './pages/member/auditSuccessMember.vue';
import memberHome from './pages/member/memberHome.vue';
import memberView from './pages/member/memberView.vue';
import memberNearby from './pages/member/memberNearby.vue';
import alreadyMember from './pages/member/alreadyMember.vue';
import reportMember from './pages/member/reportMember.vue';

// 聚会分类
import actAddSelectType from './pages/actType/actAddSelectType.vue';
import actTypeList from './pages/actType/actTypeList.vue';

// 聚会
import actCoverUpload from './pages/activity/actCoverUpload.vue';
import actAdd from './pages/activity/actAdd.vue';
import actAddDesc from './pages/activity/actAddDesc.vue';

import actDetail from './pages/activity/actDetail.vue';
import actDetailPre from './pages/activity/actDetailPre.vue';

import joinActList from './pages/activity/joinActList.vue';

import playbackActEdit from './pages/activity/playbackActEdit.vue';
import playbackActPre from './pages/activity/playbackActPre.vue';
import playbackActView from './pages/activity/playbackActView.vue';

import actHome from './pages/activity/actHome.vue';
import actNearby from './pages/activity/actNearby.vue';
import actTop from './pages/activity/actTop.vue';
import actWeekend from './pages/activity/actWeekend.vue';
import actZone from './pages/activity/actZone.vue';
import actDrivingZone from './pages/activity/actDrivingZone.vue';

import actApplySuccess from './pages/activity/actApplySuccess.vue';
import actSubmitSuccess from './pages/activity/actSubmitSuccess.vue';

import uploadQRCode from './pages/activity/uploadQRCode.vue';
import qRCodeView from './pages/activity/qRCodeView.vue';


import notPassActList from './pages/activity/notPassActList.vue';
import toAuditActList from './pages/activity/toAuditActList.vue';
import holdActList from './pages/activity/holdActList.vue';
import auditPassActList from './pages/activity/auditPassActList.vue';

import applyNotPassActList from './pages/activity/applyNotPassActList.vue';
import applySuccessActList from './pages/activity/applySuccessActList.vue';
import applyToAuditActList from './pages/activity/applyToAuditActList.vue';
// 搜索
import searchActResult from './pages/activity/searchActResult.vue';


// 用户中心
import appearanceMark from './pages/user/appearanceMark.vue';
import collectList from './pages/user/collectList.vue';
import fansList from './pages/user/fansList.vue';
import photoUpload from './pages/user/photoUpload.vue';
import userCenter from './pages/user/userCenter.vue';
import userEdit from './pages/user/userEdit.vue';
import userHome from './pages/user/userHome.vue';
import photoView from './pages/user/photoView.vue';
import blackList from './pages/user/blackList.vue';
import userActMenu from './pages/user/userActMenu.vue';
import headUpload from './pages/user/headUpload.vue';

import faceUpload from './pages/user/faceUpload.vue';
import faceResult from './pages/user/faceResult.vue';

// 注册登录
import registerOne from './pages/login/registerOne.vue';
import registerTwo from './pages/login/registerTwo.vue';
import registerThree from './pages/login/registerThree.vue';
import registerThreeUpload from './pages/login/registerThreeUpload.vue';
import registerThreeFaceResult from './pages/login/registerThreeFaceResult.vue';
import registerFour from './pages/login/registerFour.vue';
import registerFive from './pages/login/registerFive.vue';
import login from './pages/login/login.vue';
import resetPwd from './pages/login/resetPwd.vue';
import resetPwdTwo from './pages/login/resetPwdTwo.vue';

// 评价
import canCommentActList from './pages/comment/canCommentActList.vue';
import commentActEdit from './pages/comment/commentActEdit.vue';
import commentListView from './pages/comment/commentListView.vue';
import commentManager from './pages/comment/commentManager.vue';
import commentMemberEdit from './pages/comment/commentMemberEdit.vue';
import receiveCommentList from './pages/comment/receiveCommentList.vue';
import commentMemberView from './pages/comment/commentMemberView.vue';


// 消息
import msgList from './pages/message/msgList.vue';
import msgSingle from './pages/message/msgSingle.vue';

 // 获取的邀约
import offerActList from './pages/actOffer/offerActList.vue';

// 广告商业
import selectAd from './pages/advertise/selectAd.vue';

// 发现
import disHome from './pages/discovery/disHome.vue';
import memberSearchResult from './pages/discovery/memberSearchResult.vue';
import memberSearch from './pages/discovery/memberSearch.vue';

export default [
//首页

    {
        path: '/',
        component: homeTabbar,
        // 解决: "在真机上路由无法匹配,不显示任何内容" 的问题
        // alias: '/index.html',
        // url: '/index.html',
        // alias: ['/index.html'],
        keepAlive: true,
        // history:true,
        // reloadCurrent:true
        // pushState:false
    },
    {
        path: '/guideHome/',
        component: guideHome,
    },
/*    {
        path: '/index.html',
        component: homeTabbar,
        // 解决: "在真机上路由无法匹配,不显示任何内容" 的问题
        alias: '/index.html',
        // alias: ['/index.html'],
        keepAlive: true,
        // history:true,
        // reloadCurrent:true
        // pushState:false
    },*/
    {
        path: '/home/',
        component: home,
    },
    {
        path: '/SearchReady/',
        component: SearchReady,
    },

    // 广场
    {
        path: '/plaza/plazaHome/',
        component: plazaHome,
    },
    {
        path: '/plaza/plazaAdd/',
        component: plazaAdd,
    },
    {
        path: '/plaza/plazaComment/',
        component: plazaComment,
    },

// 群
    {
        path: '/group/groupAdd/',
        component: groupAdd,
    },
    {
        path: '/group/groupAddDesc/',
        component: groupAddDesc,
    },
    {
        path: '/group/groupCoverUpload/',
        component: groupCoverUpload,
    },
    {
        path: '/group/groupDetailPre/',
        component: groupDetailPre,
    },
    {
        path: '/group/groupList/',
        component: groupList,
    },
    {
        path: '/group/groupTypeList/',
        component: groupTypeList,
    },
    {
        path: '/group/myGroupList/',
        component: myGroupList,
    },
    {
        path: '/group/groupDetail/:groupId/',
        component: groupDetail,
    },


// 聚会分类
    {
        path: '/actType/actAddSelectType/',
        component: actAddSelectType,
    },
    {
        path: '/actType/actTypeList/',
        component: actTypeList,
    },

// 聚会
    {
        path: '/activity/actHome/',
        component: actHome,
    },
    {
        path: '/activity/actAdd/',
        component: actAdd,
    },
    {
        path: '/activity/actCoverUpload/',
        component: actCoverUpload,
    },
    {
        path: '/activity/actAddDesc/',
        component: actAddDesc,
    },
    {
        path: '/activity/actDetail/:actId/',
        component: actDetail,
    },
    {
        path: '/activity/actDetailPre/',
        component: actDetailPre,
    },
    {
        path: '/activity/playbackActEdit/',
        component: playbackActEdit,
    },
    {
        path: '/activity/playbackActPre/',
        component: playbackActPre,
    },
    {
        path: '/activity/playbackActView/',
        component: playbackActView,
    },
    {
        path: '/activity/joinActList/',
        component: joinActList,
    },
    {
        path: '/activity/actNearby/',
        component: actNearby,
    },
    {
        path: '/activity/actTop/',
        component: actTop,
    },
    {
        path: '/activity/actWeekend/',
        component: actWeekend,
    },
    {
        path: '/activity/actZone/',
        component: actZone,
    },
    {
        path: '/activity/actApplySuccess/',
        component: actApplySuccess,
    },
    {
        path: '/activity/actSubmitSuccess/',
        component: actSubmitSuccess,
    },
    {
        path: '/activity/qRCodeView/',
        component: qRCodeView,
    },
    {
        path: '/activity/uploadQRCode/',
        component: uploadQRCode,
    },
    {
        path: '/activity/actDrivingZone/',
        component: actDrivingZone,
    },

    // 我发起的
    {
        path: '/activity/notPassActList/',
        component: notPassActList,
    },
    {
        path: '/activity/toAuditActList/',
        component: toAuditActList,
    },
    {
        path: '/activity/holdActList/',
        component: holdActList,
    },
    {
        path: '/activity/auditPassActList/',
        component: auditPassActList,
    },
//我参加的
    {
        path: '/activity/applyNotPassActList/',
        component: applyNotPassActList,
    },
    {
        path: '/activity/applySuccessActList/',
        component: applySuccessActList,
    },
    {
        path: '/activity/applyToAuditActList/',
        component: applyToAuditActList,
    },

// 搜索结果页面
    {
        path: '/activity/searchActResult/',
        component: searchActResult,
    },

// 会员
    {
        path: '/member/auditMember/',
        component: auditMember,
    },
    {
        path: '/member/auditSuccessMember/',
        component: auditSuccessMember,
    },
    {
        path: '/member/memberHome/',
        component: memberHome,
    },
    {
        path: '/member/memberView/:memberId/',
        component: memberView,
        reloadCurrent:true
    },
    {
        path: '/member/memberNearby/',
        component: memberNearby,
    },
    {
        path: '/member/alreadyMember/',
        component: alreadyMember,
    },
    {
        path: '/member/reportMember/',
        component: reportMember,
    },

//评价
    {
        path: '/comment/canCommentActList/',
        component: canCommentActList,
    },
    {
        path: '/comment/commentActEdit/',
        component: commentActEdit,
    },
    {
        path: '/comment/commentListView/',
        component: commentListView,
    },
    {
        path: '/comment/commentManager/',
        component: commentManager,
    },
    {
        path: '/comment/commentMemberEdit/',
        component: commentMemberEdit,
    },
    {
        path: '/comment/receiveCommentList/',
        component: receiveCommentList,
    },
    {
        path: '/comment/commentMemberView/',
        component: commentMemberView,
    },


// 用户中心
    {
        path: '/user/appearanceMark/',
        component: appearanceMark,
    },
    {
        path: '/user/collectList/',
        component: collectList,
    },
    {
        path: '/user/fansList/',
        component: fansList,
    },
    {
        path: '/user/userCenter/',
        component: userCenter,
    },
    {
        path: '/user/userEdit/',
        component: userEdit,
    },
    {
        path: '/user/userHome/',
        component: userHome,
    },
    {
        path: '/user/photoUpload/',
        component: photoUpload,
    },
    {
        path: '/user/photoView/',
        component: photoView,
    },
    {
        path: '/user/blackList/',
        component: blackList,
    },
    {
        path: '/user/userActMenu/',
        component: userActMenu,
    },
    {
        path: '/user/headUpload/',
        component: headUpload,
    },
    {
        path: '/user/faceUpload/',
        component: faceUpload,
    },
    {
        path: '/user/faceResult/',
        component: faceResult,
    },

// 登录模块
    {
        path: '/login/registerOne/',
        component: registerOne,
    },
    {
        path: '/login/registerTwo/',
        component: registerTwo,
    },
    {
        path: '/login/registerThree/',
        component: registerThree,
    },
    {
        path: '/login/registerThreeUpload/',
        component: registerThreeUpload,
    },
    {
        path: '/login/registerThreeFaceResult/',
        component: registerThreeFaceResult,
    },
    {
        path: '/login/registerFour/',
        component: registerFour,
    },
    {
        path: '/login/registerFive/',
        component: registerFive,
    },

    {
        path: '/login/login/',
        component: login,
    },
    {
        path: '/login/resetPwd/',
        component: resetPwd,
    },
    {
        path: '/login/resetPwdTwo/',
        component: resetPwdTwo,
    },

    // actoffer
    {
        path: '/actOffer/offerActList/',
        component: offerActList,
    },

// common
    {
        path: '/citySelect/',
        component: citySelect,
    },
    {
        path: '/cityCheck/',
        component: cityCheck,
    },
    {
        path: '/common/locationSelect/',
        component: locationSelect,
    },

//推广商家
    {
        path: '/advertise/selectAd/',
        component: selectAd,
    },

    //消息
    {
        path: '/message/msgList/',
        component: msgList,
    },
    {
        path: '/message/msgSingle/',
        component: msgSingle,
        options: {
            props: {
                memberIdTo: 'memberIdTo'
            },
        },
    },

    //通知
    {
        path: '/notify/notifyList/',
        component: notifyList,
    },

    // 发现
    {
        path: '/discovery/disHome/',
        component: disHome,
    },
    {
        path: '/discovery/memberSearchResult/',
        component: memberSearchResult,
    },
    {
        path: '/discovery/memberSearch/',
        component: memberSearch,
    },


// 系统
    {
        path: '/system/about/',
        component: about,
    },
    {
        path: '/system/feedback/',
        component: feedback,
    },
    {
        path: '/system/settingList/',
        component: settingList,
    },
    {
        path: '/system/changeTheme/',
        component: changeTheme,
    },
    {
        path: '(.*)',
        component: notFound,
        // component: homeTabbar,
        // 解决: "在真机上路由无法匹配,不显示任何内容" 的问题
        // alias: '/index.html',
        // alias: ['/index.html', '/index.html/', '/#/index.html', '#/index.html'],
        // keepAlive: true,
    },

/*    {
        path: '/dynamic-route/blog/:blogId/post/:postId/',
        component: DynamicRoutePage,
    },*/


];
