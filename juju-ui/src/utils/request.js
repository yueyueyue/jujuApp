import axios from 'axios'
import store from '../store'
import {getToken} from '../utils/auth'
import {getServerPath} from '../utils/global'
import {toast} from '../utils/toast'

// 创建axios实例
const service = axios.create({
    // baseURL: process.env.BASE_API, // api的base_url
    baseURL: getServerPath(),
    // baseURL :'http://10.0.62.23:8080/',
    timeout: 1500000 // 请求超时时间
})

// request拦截器
service.interceptors.request.use(config => {
    // if (store.getters.token) {
    config.headers['Authorization'] = getToken() // 让每个请求携带自定义token 请根据实际情况自行修改
    /*  config.headers = {
        'Content-Type': 'application/json' // 设置很关键
      }*/
    // }
    return config
}, error => {
    // Do something with request error
    console.log(error) // for debug
    // Promise.reject(error)
})

// respone拦截器
service.interceptors.response.use(
    response => {
        /**
         * code为非200是抛错 可结合自己业务进行修改
         */
        const res = response.data
        console.log(res);
        if (res.code == 301) {
            // alert("请先登录或注册~~");
            return response.data
        } else if (res.code == 0) {
            /*      Message({
                    message: res.message,
                    type: 'error',
                    duration: 3 * 1000
                  })*/

            // 401:未登录;
            // if (res.code === 401||res.code === 403) {
            /*        MessageBox.confirm('你已被登出，可以取消继续留在该页面，或者重新登录', '确定登出', {
                      confirmButtonText: '重新登录',
                      cancelButtonText: '取消',
                      type: 'warning'
                    }).then(() => {
                      store.dispatch('FedLogOut').then(() => {
                        location.reload()// 为了重新实例化vue-router对象 避免bug
                      })
                    })*/
            // }
            // return Promise.reject('error')
            return response.data
        } else {
            return response.data
        }
    },
    error => {
        toast("哎呀，网络开小差了~~", 1500);
        console.log('err' + error)// for debug
        /*    Message({
              message: error.message,
              type: 'error',
              duration: 3 * 1000
            })*/
        // return Promise.reject(error)
    }
)

export default service
