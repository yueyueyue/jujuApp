import Cookies from 'js-cookie'

// face专用
export function imageShowCustom(result,maxSize,quality, callback) {
    // 如大于3MB,则压缩
    if (result.length > maxSize) {
        let quality = quality;
        let img = new Image();
        img.src = result;
        console.log("********未压缩前的图片大小********");
        console.log(result.length);
        img.onload = function () {
            let data = compress(img, quality);
            console.log("data=" + data);
            return callback(data);
        };
    } else {
        return callback(result);
    }
}

/**
 * 图片展示，是否压缩
 * @param result
 * @param callback
 * @returns {*}
 */
export function imageShow(result, callback) {
    // 如大于3MB,则压缩
    if (result.length > 3000000) {
        let quality = 0.5;
        let img = new Image();
        img.src = result;
        console.log("********未压缩前的图片大小********");
        console.log(result.length);
        img.onload = function () {
            let data = compress(img, quality);
            console.log("data=" + data);
            return callback(data);
        };
    } else {
        return callback(result);
    }
}

/**
 * 压缩图片
 * img file对象
 * quality 图片质量
 */
export function compress(img, quality) {
    let canvas = document.createElement("canvas");
    let ctx = canvas.getContext("2d");
    let initSize = img.src.length;
    console.log("原始initSize=" + initSize);
    let width = img.width;
    let height = img.height;
    canvas.width = width;
    canvas.height = height;
    // 铺底色
    ctx.fillStyle = "#fff";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(img, 0, 0, width, height);

    //进行最小压缩
    let ndata = canvas.toDataURL("image/jpeg", quality);
    console.log("*******压缩后的图片大小*******");
    console.log(ndata)
    console.log(ndata.length);
    return ndata;
}

// base64转成bolb对象
export function dataURItoBlob(base64Data) {
    var byteString;
    if (base64Data.split(",")[0].indexOf("base64") >= 0)
        byteString = atob(base64Data.split(",")[1]);
    else byteString = unescape(base64Data.split(",")[1]);
    var mimeString = base64Data
        .split(",")[0]
        .split(":")[1]
        .split(";")[0];
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], {type: mimeString});
}





