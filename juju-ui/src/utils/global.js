import Cookies from "js-cookie";

const isApp = 0;

/**
 * 全局变量
 * @type {string}
 */

// 线上环境
/*const basePath="https://www.23juju.com/";
const serverPath = basePath;
const cdnPath = basePath + "cdn/";
const serverWS = "wss://www.23juju.com/wss/im/";*/

// 测试环境
const basePath = "http://192.168.8.161";
// const basePath = "http://10.0.62.1";
const serverPath = basePath + ":8080/";
const cdnPath = basePath + "/cdn/";
// const serverWS = "ws://10.0.62.1:8080/wss/im/";
const serverWS = "ws://192.168.8.161:8080/wss/im/";

// cdn地址
const cdnPathAppAvatar = cdnPath + "appAvatar/";

const cdnPathActType = cdnPath + "actType/";
const cdnPathAd = cdnPath + "ad/";
const cdnPathActCoverImg = cdnPath + "actCoverImg/";
const cdnPathPhoto = cdnPath + "photo/";
const cdnPathFaceImg = cdnPath + "faceImg/";

const cdnPathGroupTypeCover = cdnPath + "groupTypeCover/";
const cdnPathGroupQR = cdnPath + "groupQR/";
const cdnPathGroupDesc = cdnPath + "groupDesc/";

const pageSize = 10;

const NULL_TIP_FOR_LISTITEM = "~~寻TA千百度，却空空如也";
const NULL_TIP_FOR_PHOTO = "~~主人还未来得及更新哦";

export function getServerPath() {
    return serverPath;
}

export default {
    isApp,
    serverPath,
    serverWS,
    cdnPath,
    cdnPathAppAvatar,
    cdnPathActType,
    cdnPathActCoverImg,
    cdnPathFaceImg,

    cdnPathPhoto,

    cdnPathGroupTypeCover,
    cdnPathGroupQR,
    cdnPathGroupDesc,

    cdnPathAd,
    pageSize,
    NULL_TIP_FOR_LISTITEM,
    NULL_TIP_FOR_PHOTO
}
