//百度地图获取城市名称的方法
import Cookies from 'js-cookie'
import store from '../store'

export function getCurrentCityName(data,params) {
    function myFun(result){
        var cityName = result.name;
        alert("当前定位城市:"+JSON.stringify(result));
        // store.state.app.locationCity=cityName;
        store.dispatch('changeLocationCity', result).then(() => {
        }).catch((e) => {
            console.log(e);
        })
        // Cookies.set("locationCity", cityName);
    }
    var myCity = new BMap.LocalCity();
    myCity.get(myFun);
    console.log(JSON.stringify(myCity));
}

export function getCurrentCityNameByApp() {
    baidumap_location.getCurrentPosition(function (result) {
        alert(JSON.stringify(result, null, 4));
        this.posText = JSON.stringify(result, null, 4);
        console.log(JSON.stringify(result, null, 4));

        store.dispatch('changeLocationCity', result.city).then(() => {
        }).catch((e) => {
            console.log(e);
        });
    }, function (error) {

    });
}


export function getLocation() {
    //获取当前位置
    var geolocation = new BMap.Geolocation();
    geolocation.getCurrentPosition(function(r) {
        if(this.getStatus() == BMAP_STATUS_SUCCESS) {
            var mk = new BMap.Marker(r.point);
            getAddress(r.point);
        } else {
            alert('failed' + this.getStatus());
        }
    });
    /*获取地址信息，设置地址label*/
    function getAddress(point) {
        var gc = new BMap.Geocoder();
        gc.getLocation(point, function(rs) {
            var addComp = rs.addressComponents;
            /*获取地址*/
            var address = addComp.province + addComp.city + addComp.district + addComp.street + addComp.streetNumber;
            console.log(address);
        });
    }
}

