export function isvalidUsername(str) {
  const valid_map = ['admin', 'test']
  return valid_map.indexOf(str.trim()) >= 0
}

/* 合法uri*/
export function validateURL(textval) {
  const urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/
  return urlregex.test(textval)
}

/* 小写字母*/
export function validateLowerCase(str) {
  const reg = /^[a-z]+$/
  return reg.test(str)
}

/* 大写字母*/
export function validateUpperCase(str) {
  const reg = /^[A-Z]+$/
  return reg.test(str)
}

/* 大小写字母*/
export function validatAlphabets(str) {
  const reg = /^[A-Za-z]+$/
  return reg.test(str)
}

/* 删除数组 */
export function arrayRemove(array,value) {
  let index = array.indexOf(value);
  if (index > -1) {
    array.splice(index, 1);
  }
}

/* 在原数组追加前缀 */
export function splitAndAppend(arrayStr, appendStr, mark) {
  let array = [];
  if (arrayStr != undefined && arrayStr != '') {
    array = arrayStr.split(mark);
  }
  for (let i = 0; i < array.length; i++) {
    array[i] = appendStr + array[i];
  }
  console.log("array=" + array.toString());
  return array;
}

// 手机号格式校验
export function validateMobile(mobile) {
  let reg = /^1[0-9]{10}$/
  if (mobile == '' || mobile.length <= 10 || !reg.test(mobile)) {
    return false;
  } else {
    return true;
  }
}


